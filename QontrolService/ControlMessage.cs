﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace QontrolService
{
    [Serializable]
    public class ControlMessage
    {
        public string Version;
        public string DataType;
        public object Data;
        public string Message;

        public ControlMessage()
        {
            Version = Maintenance.CurrentVersion.ToString();
        }
    }

    [Serializable]
    public class SystemStateData
    {
        public string MachineName = null;
        public string MachineDomain;
        public string UserName;

        public string ProcessorName;
        public short NumberOfCpuCores;

        public string MotherboardVendor;
        public string MotherboardModel;
        public string BiosInfo;

        public ushort CpuLoad;
        public ushort MemoryLoad;
        public ushort TotalMemory;
        public ushort AvailableMemory;

        public string OSVersion;
        public bool UsbOn;
        public string Gateway;


        public override string ToString()
        {
            return String.Format("MN={0}; DN={1}; UN={2}; CL={3}%; ML={4}%; USB={5}; GW={6}", 
                MachineName, MachineDomain, UserName, CpuLoad, MemoryLoad, UsbOn, Gateway);
        }

        public SystemStateData()
        {

        }

        public void UpdateHardwareInfo()
        {
            MachineName = Environment.MachineName;
            MachineDomain = Environment.UserDomainName; // служба работает от имени системы, а домен системы вряд ли отличается от домена компа
            OSVersion = Environment.OSVersion.ToString();
            Hardware.UpdateSystemInfo(this);
        }

        public void UpdateValues()
        {
            if (MachineName == null)
                UpdateHardwareInfo();
            UserName = ServiceTools.GetInteractiveUserName() ?? Environment.UserName;
            Hardware.UpdatePerformanceData(this);
            
            UsbOn = (int)Registry.LocalMachine
                .OpenSubKey(@"SOFTWARE\Policies\Microsoft\Windows\RemovableStorageDevices", false)
                .GetValue("Deny_All", 0x0) != 1;
        }
    }

    [Serializable]
    public class UpdateInfoData
    {
        public string InstallerPath;
        public string InstallerArgs;

        public UpdateInfoData(string installer, string args)
        {
            InstallerPath = installer;
            InstallerArgs = args;
        }
    }
}
