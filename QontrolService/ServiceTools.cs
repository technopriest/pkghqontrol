﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;

namespace QontrolService
{
    public static class ServiceTools
    {
        [DllImport("kernel32", SetLastError = true)]
        private static extern Boolean CloseHandle(IntPtr handle);

        [DllImport("wtsapi32.dll", SetLastError = true)]
        private static extern bool WTSQueryUserToken(UInt32 sessionId, out IntPtr token);

        [StructLayout(LayoutKind.Sequential)]
        private struct STARTUPINFO
        {
            public Int32 cb;
            public String lpReserved;
            public String lpDesktop;
            public String lpTitle;
            public Int32 dwX;
            public Int32 dwY;
            public Int32 dwXSize;
            public Int32 dwYSize;
            public Int32 dwXCountChars;
            public Int32 dwYCountChars;
            public Int32 dwFillAttribute;
            public Int32 dwFlags;
            public Int16 wShowWindow;
            public Int16 cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public Int32 dwProcessId;
            public Int32 dwThreadId;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SECURITY_ATTRIBUTES
        {
            public int nLength;
            public IntPtr lpSecurityDescriptor;
            public int bInheritHandle;
        }

        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern bool CreateProcessAsUser(
            IntPtr hToken,
            string lpApplicationName,
            string lpCommandLine,
            ref SECURITY_ATTRIBUTES lpProcessAttributes,
            ref SECURITY_ATTRIBUTES lpThreadAttributes,
            bool bInheritHandles,
            uint dwCreationFlags,
            IntPtr lpEnvironment,
            string lpCurrentDirectory,
            ref STARTUPINFO lpStartupInfo,
            out PROCESS_INFORMATION lpProcessInformation);

        [DllImport("userenv.dll", SetLastError = true)]
        private static extern bool CreateEnvironmentBlock(
                ref IntPtr lpEnvironment,
                IntPtr hToken,
                bool bInherit);

        private static IntPtr GetEnvironmentBlock(IntPtr token)
        {
            IntPtr envBlock = IntPtr.Zero;
            bool retVal = CreateEnvironmentBlock(ref envBlock, token, false);
            if (retVal == false)
            {
                string message = String.Format("CreateEnvironmentBlock Error: {0}", Marshal.GetLastWin32Error());
                throw new Exception(message);
            }
            return envBlock;
        }

        [DllImport("wtsapi32.dll", CharSet = CharSet.Auto)]
        private static extern bool WTSEnumerateSessions(
            IntPtr hServer,
            // Not required, but a good practice.
            [MarshalAs(UnmanagedType.U4)]
            int Reserved,
            [MarshalAs(UnmanagedType.U4)]
            int Version,
            // You are going to create the memory block yourself.
            ref IntPtr ppSessionInfo,
            [MarshalAs(UnmanagedType.U4)]
            ref int pCount);

        [StructLayout(LayoutKind.Sequential)]
        private struct WTS_SESSION_INFO
        {
            public int SessionId;
            public string pWinStationName;
            public WTS_CONNECTSTATE_CLASS State;
        }

        private enum WTS_CONNECTSTATE_CLASS
        {
            WTSActive,
            WTSConnected,
            WTSConnectQuery, // This was misspelled.
            WTSShadow,
            WTSDisconnected,
            WTSIdle,
            WTSListen,
            WTSReset,
            WTSDown,
            WTSInit
        };

        public enum SHOW_WINDOW
        {
            /// <summary>
            /// Hides the window and activates another window.
            /// </summary>
            SW_HIDE = 0,
            /// <summary>
            /// Activates and displays a window. If the window is minimized or maximized, the system restores it to its original size and position. An application should specify this flag when displaying the window for the first time.
            /// </summary>
            SW_SHOWNORMAL,
            /// <summary>
            /// Activates the window and displays it as a minimized window.
            /// </summary>
            SW_SHOWMINIMIZED,
            /// <summary>
            /// Activates the window and displays it as a maximized window.
            /// </summary>
            SW_SHOWMAXIMIZED,
            /// <summary>
            /// Displays a window in its most recent size and position. This value is similar to SW_SHOWNORMAL, except that the window is not activated.
            /// </summary>
            SW_SHOWNOACTIVATE,
            /// <summary>
            /// Activates the window and displays it in its current size and position.
            /// </summary>
            SW_SHOW,
            /// <summary>
            /// Minimizes the specified window and activates the next top-level window in the Z order.
            /// </summary>
            SW_MINIMIZE,
            /// <summary>
            /// Displays the window as a minimized window. This value is similar to SW_SHOWMINIMIZED, except the window is not activated.
            /// </summary>
            SW_SHOWMINNOACTIVE,
            /// <summary>
            /// Displays the window in its current size and position. This value is similar to SW_SHOW, except that the window is not activated.
            /// </summary>
            SW_SHOWNA,
            /// <summary>
            /// Activates and displays the window. If the window is minimized or maximized, the system restores it to its original size and position. An application should specify this flag when restoring a minimized window.
            /// </summary>
            SW_RESTORE,
            /// <summary>
            /// Sets the show state based on the SW_ value specified in the STARTUPINFO structure passed to the CreateProcess function by the program that started the application.
            /// </summary>
            SW_SHOWDEFAULT,
            /// <summary>
            /// Minimizes a window, even if the thread that owns the window is not responding. This flag should only be used when minimizing windows from a different thread.
            /// </summary>
            SW_FORCEMINIMIZE
        }

        [DllImport("wtsapi32.dll")]
        private static extern void WTSFreeMemory(IntPtr pMemory);

        [DllImport("userenv.dll", SetLastError = true)]
        private static extern bool DestroyEnvironmentBlock(
                IntPtr lpEnvironment);

        public static IntPtr GetInteractiveUserToken()
        {
            IntPtr hToken = IntPtr.Zero;
            IntPtr buffer = IntPtr.Zero;
            int count = 0;
            uint sessionId = 0xFFFFFFFF;
            if (WTSEnumerateSessions(IntPtr.Zero, 0, 1, ref buffer, ref count))
            {
                WTS_SESSION_INFO[] sessionInfo = new WTS_SESSION_INFO[count];
                for (int index = 0; index < count; index++)
                {
                    sessionInfo[index] = (WTS_SESSION_INFO)Marshal.PtrToStructure(
                        (IntPtr)(buffer.ToInt32() + Marshal.SizeOf(sessionInfo[0]) * index), typeof(WTS_SESSION_INFO));
                    if (sessionInfo[index].State == WTS_CONNECTSTATE_CLASS.WTSActive)
                    {
                        sessionId = (uint)sessionInfo[index].SessionId;
                        break;
                    }
                }
            }
            WTSFreeMemory(buffer);
            if (sessionId == 0xFFFFFFFF)
                throw new InvalidOperationException("Отсутствует активная сессия рабочего стола.");
            if (!WTSQueryUserToken(sessionId, out hToken))
                throw new InvalidOperationException("WTSQueryUserToken error #" + Marshal.GetLastWin32Error());
            return hToken;
        }

        public static string GetInteractiveUserName()
        {
            IntPtr hToken = IntPtr.Zero;
            try 
            { 
                hToken = GetInteractiveUserToken();
                var identity = new WindowsIdentity(hToken);
                CloseHandle(hToken);
                return identity.Name;
            }
            catch
            {
                return null;
            }
        }

        public static void ExecuteAsInteractiveUser(string command, SHOW_WINDOW displayMode = SHOW_WINDOW.SW_SHOW)
        {
            IntPtr hToken = GetInteractiveUserToken();
            ExecuteAsUser(command, hToken);
            CloseHandle(hToken);
        }

        public static int ExecuteAsUser(string command, IntPtr hToken, SHOW_WINDOW displayMode = SHOW_WINDOW.SW_SHOW)
        {
            PROCESS_INFORMATION pi = new PROCESS_INFORMATION();
            STARTUPINFO si = new STARTUPINFO();
            IntPtr hEnvironment = IntPtr.Zero;
            try
            {
                si.cb = Marshal.SizeOf(si);
                si.lpDesktop = null;
                si.dwFlags = 0x00000040 | 0x00000001;
                si.wShowWindow = (short)displayMode;
                SECURITY_ATTRIBUTES saProcess = new SECURITY_ATTRIBUTES();
                SECURITY_ATTRIBUTES saThread = new SECURITY_ATTRIBUTES();
                saProcess.nLength = (int)Marshal.SizeOf(saProcess);
                saThread.nLength = (int)Marshal.SizeOf(saThread);
                hEnvironment = GetEnvironmentBlock(hToken);
                if (!CreateProcessAsUser(hToken, null, command, ref saProcess, ref saThread, false,
                    0x04000000 | 0x00000010 | 0x00000200 | 0x00000400, hEnvironment, null, ref si, out pi))
                {
                    throw new Exception("CreateProcessAsUser error #" + Marshal.GetLastWin32Error());
                }
            }
            finally
            {
                DestroyEnvironmentBlock(hEnvironment);
                CloseHandle(pi.hProcess);
                CloseHandle(pi.hThread);
            }
            return pi.dwProcessId;
        }
    }
}
