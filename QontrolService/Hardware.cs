﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace QontrolService
{
    public class Hardware
    {
        private static PerformanceCounter _cpuLoadCounter = null;

        public static PerformanceCounter CpuLoadCounter
        {
            get
            {
                return _cpuLoadCounter ?? (_cpuLoadCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total"));
            }
        }

        public static void UpdatePerformanceData(SystemStateData systemStatus)
        {
            var memoryStatus = new MEMORYSTATUSEX();
            if (GlobalMemoryStatusEx(memoryStatus))
            {
                systemStatus.MemoryLoad = (ushort)memoryStatus.dwMemoryLoad;
                systemStatus.TotalMemory = (ushort)(memoryStatus.ullTotalPhys >> 20);
                systemStatus.AvailableMemory = (ushort)(memoryStatus.ullAvailPhys >> 20);
                systemStatus.CpuLoad = (ushort)CpuLoadCounter.NextValue();
            }
        }

        public static void UpdateSystemInfo(SystemStateData systemStatus)
        {
            using (var systemKey = Registry.LocalMachine.OpenSubKey(@"HARDWARE\DESCRIPTION\System", false))
            {
                using (var biosKey = systemKey.OpenSubKey("BIOS", false))
                {
                    systemStatus.MotherboardVendor = biosKey.GetValue("BaseBoardManufacturer") as string;
                    systemStatus.MotherboardModel = biosKey.GetValue("BaseBoardProduct") as string;
                    systemStatus.BiosInfo = String.Format("{0}; {1}; {2}", 
                        biosKey.GetValue("BIOSVersion"), 
                        biosKey.GetValue("BIOSReleaseDate"), 
                        biosKey.GetValue("BIOSVendor"));
                }
                using (var cpuKey = systemKey.OpenSubKey("CentralProcessor", false))
                {
                    systemStatus.NumberOfCpuCores = (short)cpuKey.GetSubKeyNames().Length;
                    using (var cpu0 = cpuKey.OpenSubKey("0", false))
                    {
                        systemStatus.ProcessorName = cpu0.GetValue("ProcessorNameString") as string;
                    }
                }
            }
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        private class MEMORYSTATUSEX
        {
            public uint dwLength;
            public uint dwMemoryLoad;
            public ulong ullTotalPhys;
            public ulong ullAvailPhys;
            public ulong ullTotalPageFile;
            public ulong ullAvailPageFile;
            public ulong ullTotalVirtual;
            public ulong ullAvailVirtual;
            public ulong ullAvailExtendedVirtual;
            public MEMORYSTATUSEX()
            {
                this.dwLength = (uint)Marshal.SizeOf(typeof(MEMORYSTATUSEX));
            }
        }
        
        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern bool GlobalMemoryStatusEx([In, Out] MEMORYSTATUSEX lpBuffer);
        
    }
}
