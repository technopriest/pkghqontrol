﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("System Health Control Service")]
[assembly: AssemblyDescription("Отправляет показатели производительности и контролирует настройки критических системных объектов")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Политехнический колледж городского хозяйства")]
[assembly: AssemblyProduct("College Computer Monitoring Solution")]
[assembly: AssemblyCopyright("Copyright © 2015 Dmitry A. Starikov")]
[assembly: AssemblyTrademark("CCMS™")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("4590fa88-bc5a-4559-a37f-0a6e9b83c917")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.160.*")]
[assembly: AssemblyFileVersion("1.150.810.43")]
[assembly: NeutralResourcesLanguageAttribute("")]
