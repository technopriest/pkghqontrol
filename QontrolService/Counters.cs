﻿using Microsoft.VisualBasic.Devices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace QontrolService
{
    public class Counters
    {
        public StatusStruct Values;

        PerformanceCounter _ramAvailable = new PerformanceCounter("Memory", "Available MBytes");
        PerformanceCounter _cpuLoad = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        ComputerInfo _computerInfo = new Microsoft.VisualBasic.Devices.ComputerInfo();

        private uint GetTotalRAM()
        {
            return (uint)(_computerInfo.TotalPhysicalMemory / 1024 / 1024);
        }

        public void FillValues()
        {
            Values.MachineName = Environment.MachineName; 
            Values.DomainName = Environment.UserDomainName;
            Values.UserName = ServiceTools.GetInteractiveUserName() ?? Environment.UserName;
            Values.MemoryAvailable = (ushort)_ramAvailable.NextValue();
            Values.MemoryTotal = (ushort)GetTotalRAM();
            Values.MemoryLoad = (ushort)((Values.MemoryTotal - Values.MemoryAvailable) / (double)Values.MemoryTotal * 100.0f);
            Values.ProcessorLoad = (ushort)_cpuLoad.NextValue();
            Values.Version = Maintenance.CurrentVersion.ToString();

            if (!String.IsNullOrWhiteSpace(Values.Message))
            {
                if (Values.MessageSent)
                {
                    Values.Message = String.Empty;
                    Values.MessageSent = false;
                }
                else
                    Values.MessageSent = true;
            }
        }

        public override string ToString()
        {
            FillValues();
            return Values.ToString();
        }
    }
}
