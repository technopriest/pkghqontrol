﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace QontrolService
{
    public interface ICompressable
    {
        byte[] ToBytes();
        void CopyToBuffer(byte[] buffer, int startIndex);
        int SizeOf { get; }
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode, Pack = 1)]
    unsafe public struct StatusStruct : ICompressable
    {
        public UInt16 ProcessorLoad;
        public UInt16 MemoryAvailable;
        public UInt16 MemoryTotal;
        public UInt16 MemoryLoad;

        public bool UsbState;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 15)]
        public string IpGateway;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string MachineName;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string DomainName;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public string UserName;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 512)]
        public string Message;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string Version;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
        public string Command;

        public fixed byte Data[1024];

        public bool MessageSent;

        public override string ToString()
        {
            return String.Format("MN={4};DN={5};UN={6};V={7};ML={1}%;MT={2};MA={3};CL={0}%",
                ProcessorLoad, MemoryLoad, MemoryTotal, MemoryAvailable,
                MachineName, DomainName, UserName, Version);
        }

        public int SizeOf { get { return sz; } }

        public static int sz = Marshal.SizeOf(typeof(StatusStruct));

        public void CopyToBuffer(byte[] buffer, int startIndex)
        {
            IntPtr ptr = Marshal.AllocHGlobal(sz);
            Marshal.StructureToPtr(this, ptr, false);
            Marshal.Copy(ptr, buffer, startIndex, sz);
            Marshal.FreeHGlobal(ptr);
        }

        public byte[] ToBytes()
        {
            byte[] buffer = new byte[sz];
            CopyToBuffer(buffer, 0);
            return buffer;
        }

        public static StatusStruct CopyFromBuffer(byte[] buffer, int startIndex)
        {
            IntPtr ptr = Marshal.AllocHGlobal(sz);
            Marshal.Copy(buffer, startIndex, ptr, sz);
            StatusStruct stockPrice =
              (StatusStruct)Marshal.PtrToStructure(ptr, typeof(StatusStruct));
            Marshal.FreeHGlobal(ptr);
            return stockPrice;
        }
    }
}
