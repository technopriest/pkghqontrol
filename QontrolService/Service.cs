﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QontrolService
{
    public class Service : ServiceBase
    {
        public Service()
        {
            ServiceName = Maintenance.AssemblyTitle;
        }

        protected override void OnStart(string[] args)
        {
            Program.ConsoleMain(args);
        }

        protected override void OnStop()
        {
            if (QontrolService.Program.QontrolServer != null)
                QontrolService.Program.QontrolServer.StopReceive();
            if (QontrolService.Program.QontrolClient != null)
                QontrolService.Program.QontrolClient.StopSendStatus();
        }
    }

    
}
