﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace QontrolService
{
    public class NetConfig
    {
        private string _ipAddress;
        private string _mask;
        private string _gateway;
        private string _dns1;
        private string _dns2;

        private string _ipFilter;
        private string _maskFilter;

        private List<InterfaceConfig> _adapters = new List<InterfaceConfig>();

        public NetConfig(string ipStart, string maskStart)
        {
            _ipFilter = ipStart;
            _maskFilter = maskStart;
        }

        public void GetValues()
        {
            string output = Helpers.ReadConsoleProcessOutput("ipconfig", "/all");
            Debug.WriteLine(output);
        }

        public void InitAdapters()
        {
            _adapters.Clear();
            bool first = true;
            StringBuilder lines = new StringBuilder(1024);
            Helpers.CreateProcessWithRedirectedOutput("ipconfig", "/all", line =>
                {
                    if (line == null) return;
                    if (line.StartsWith("Ethernet adapter "))
                    {
                        if (!first)
                        {
                            _adapters.Add(InterfaceConfig.ParseIpConfig(lines.ToString()));
                            lines.Clear();
                        }
                        else
                        {
                            lines.Clear();
                            first = false;
                        }
                    }
                    lines.AppendLine(line);
                }, true);
        }
    }

    public class InterfaceConfig
    {
        public string InterfaceName { get; set; }
        public string DeviceName { get; set; }
        public string MacAddress { get; set; }
        public List<string> IpAddressList { get; set; }
        public List<string> NetMaskList { get; set; }
        public List<string> DnsList { get; set; }
        public bool DHCP { get; set; }
        public InterfaceConfig()
        {
            IpAddressList = new List<string>();
            NetMaskList = new List<string>();
            DnsList = new List<string>();
        }

        public void Parse(string output)
        {
            IpAddressList.Clear();
            NetMaskList.Clear();
            DnsList.Clear();
            InterfaceName = output.RegexMatch(@"Ethernet adapter (.*?):");
            MacAddress = output.RegexMatch(@": ((\w{2}\-){5}\w{2})");
            var dhcp = output.RegexMatch("@DHCP.*?: (.*");
        }

        public static InterfaceConfig ParseIpConfig(string output)
        {
            var result = new InterfaceConfig();
            result.Parse(output);
            return result;
        }
    }
}
