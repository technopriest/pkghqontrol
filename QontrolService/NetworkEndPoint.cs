﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace QontrolService
{
    public class NetworkEndPoint
    {
        public IPEndPoint IpEndpoint;
        public PhysicalAddress MacAddress;
        public Version ProgramVersion;

        public IPAddress IpAddress
        {
            get { return IpEndpoint.IfNotNull(ipe => ipe.Address); }
        }

        public NetworkEndPoint(IPEndPoint ipEndpoint, PhysicalAddress macAddress)
        {
            this.IpEndpoint = ipEndpoint;
            this.MacAddress = macAddress;
        }

        public NetworkEndPoint(IPAddress ipAddress, PhysicalAddress macAddress)
        {
            this.IpEndpoint = new IPEndPoint(ipAddress, 0);
            this.MacAddress = macAddress;
        }

        public NetworkEndPoint(IPEndPoint ipEndpoint)
        {
            this.IpEndpoint = ipEndpoint;
            this.MacAddress = ArpTableHelper.GetMacByIP(IpEndpoint.Address, true);
        }

        public override string ToString()
        {
            return String.Format("SRC <{0}> MAC <{1}> VER <{2}>", IpEndpoint, MacAddress, ProgramVersion);
        }

        public bool WakeUp()
        {
            return WakeOnLan(this.MacAddress);
        }

        #region Wake On Lan functionality

        private static UdpClient _wolUdpClient;

        private static UdpClient GetWOLClient()
        {
            if (_wolUdpClient != null)
                return _wolUdpClient;
            _wolUdpClient = new UdpClient();
            _wolUdpClient.Connect(new IPAddress(0xffffffff), 0x2fff);
            try
            {
                _wolUdpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 0);
            }
            catch
            {
                return null;
            }
            return _wolUdpClient;
        }

        public static bool WakeOnLan(PhysicalAddress macAddress)
        {
            var udpClient = GetWOLClient();
            if (udpClient == null || macAddress == null)
                return false;

            var macBytes = macAddress.GetAddressBytes();
            if (macBytes.Length < 6)
                return false;

            int byteCount = 0;
            byte[] packet = new byte[102];
            // Первые 6 байт пакета должны быть 0xFF
            for (int i = 0; i < 6; i++)
                packet[byteCount++] = 0xff;
            // Повторяем MAC 16 раз
            for (int i = 0; i < 16; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    packet[byteCount++] = macBytes[j];
                }
            }
            var bytesSend = udpClient.Send(packet, byteCount);
            if (bytesSend != byteCount)
                return false;
            return true;
        }

        #endregion
    }


}
