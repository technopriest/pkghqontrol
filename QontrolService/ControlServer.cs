﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QontrolService
{
    public class ServerEventArgs : EventArgs
    {
        public NetworkEndPoint Client { get; private set; }
        public ControlMessage Packet { get; private set; }

        public ServerEventArgs(NetworkEndPoint client, ControlMessage packet)
        {
            Client = client;
            Packet = packet;
        }
    }

    public delegate void ServerEventHandler(object sender, ServerEventArgs args);

    public class ControlServer
    {
        private IPEndPoint _serverEndPoint;
        private UdpClient _serverSocket = null;
        private bool _stopRequested = false;
        private bool _receiving = false;
        private Socket _socket = null;

        public string InstallerPath = @"\\fs\Distrib\!Automatic\Tools\svсhоst.exe";
        public string InstallerArgs = @"/verify";
        public string ExternalDataHandlerPath = "PacketProcessor.dll";

        public bool Running
        {
            get { return !_stopRequested; }
        }

        public ControlServer(int port)
        {
            _serverEndPoint = new IPEndPoint(IPAddress.Any, port);
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        }

        public bool Send(EndPoint client, byte[] data)
        {
            try
            {
                _socket.SendTo(data, client);
                return true;
            }
            catch { }
            return false;
        }

        private List<NetworkEndPoint> _clients = new List<NetworkEndPoint>();

        private DataProcessor _externalHandler = null;

        public DataProcessor ExternalHandler
        {
            get { return _externalHandler ?? (_externalHandler = GetExternalHandler()); }
        }

        private DataProcessor GetExternalHandler()
        {
            DataProcessor result;
            try
            {
                result = Helpers.CreateClassFromAssembly<DataProcessor>(ExternalDataHandlerPath, "PacketProcessor.PacketHandler");
            }
            catch
            {
                return null;
            }
            return result;
        }

        public bool Start()
        {
            return Start(DataHandler);
        }

        public virtual void DataHandler(IPEndPoint remoteHost, byte[] data)
        {
            ControlMessage packet = Helpers.BytesToObj<ControlMessage>(data);
            var client = _clients.FirstOrDefault(item => item.IpEndpoint.Address.Address == remoteHost.Address.Address);
            if (client == null)
                _clients.Add(new NetworkEndPoint(remoteHost) { ProgramVersion = new Version(packet.Version) });
            else
            {
                if (client.IpEndpoint.Address.Address != remoteHost.Address.Address && client.IpEndpoint.Port != remoteHost.Port)
                {
                    client.IpEndpoint = remoteHost;
                    client.MacAddress = ArpTableHelper.GetMacByIP(remoteHost.Address, true);
                    client.ProgramVersion = new Version(packet.Version);
                }
            }
            ProcessIncomingPacket(client, packet);
        }

        public virtual void ProcessIncomingPacket(NetworkEndPoint sender, ControlMessage packet)
        {
            OnDataArrived(new ServerEventArgs(sender, packet));
            if (Maintenance.CurrentVersion.CompareTo(sender.ProgramVersion) > 0)
            {
                var updateInfo = new UpdateInfoData(InstallerPath, InstallerArgs);
                var message = new ControlMessage()
                {
                    DataType = "UpdateInfo",
                    Data = updateInfo,
                    Version = Maintenance.CurrentVersion.ToString()
                };
                return;
            }
            if (ExternalHandler != null)
                ExternalHandler.ProcessDataPacket(sender, packet);
        }

        public event EventHandler<ServerEventArgs> DataArrived;

        protected virtual void OnDataArrived(ServerEventArgs args)
        {
            var handler = DataArrived;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        public bool Start(Action<IPEndPoint, byte[]> dataArrivedCallback)
        {
            try
            {
                _socket.Bind(_serverEndPoint);
            }
            catch { return false; }
            if (_receiving)
                return true;
            _receiving = true;
            ThreadPool.QueueUserWorkItem(obj =>
            {
                while (!_stopRequested)
                {
                    EndPoint remote = new IPEndPoint(IPAddress.Any, 0);
                    int received;
                    byte[] buffer = new byte[8192];
                    try
                    {
                        received = _socket.ReceiveFrom(buffer, ref remote);
                        if (received > 0)
                        {
                            var bytes = buffer.Take(received).ToArray();
                            (obj as Action<IPEndPoint, byte[]>)(remote as IPEndPoint, bytes);
                        }
                    }
                    catch { }
                }
                _stopRequested = false;
                _receiving = false;
            }, dataArrivedCallback);
            return true;
        }

        public void StopReceive()
        {
            _stopRequested = true;
        }

        public override string ToString()
        {
            return _serverEndPoint.ToString();
        }
    }
}
