﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace QontrolService
{
    public class ControlClientEventArgs : EventArgs
    {
        public ControlMessage Packet { get; set; }
    }

    public class ControlClient
    {
        private UdpClient _client = null;
        private string _host;
        private int _port;
        
        private bool _sending = false;

        private bool _stopRequested = false;

        private int _delay = 1000;

        public bool Running
        {
            get { return !_stopRequested; }
        }

        public ControlClient(string host, int port, int delay = 1000)
        {
            _host = host;
            _port = port;
            _delay = delay;
        }

        private bool CheckUdpClient()
        {
            if (_client != null)
                return true;
            try
            {
                _client = new UdpClient(_host, _port);
            }
            catch { _client = null; }
            return _client != null;
        }

        public bool Init()
        {
            if (!CheckUdpClient())
                return false;
            StartReplyListener(ReplyHandler);
            BeginSendStatus();
            return true;
        }

        private void ReplyHandler(byte[] bytes)
        {
            var packet = Helpers.BytesToObj<ControlMessage>(bytes);
            OnDataArrived(packet);
            if (packet.DataType == "UpdateInfo")
            {
                UpdateInfoData data = packet.Data as UpdateInfoData;
                if (data != null)
                {
                    packet.Message = String.Format("Upgrading from {0} to {1}...",
                        Maintenance.CurrentVersion, packet.Version);
                    SendPacket(packet);
                    if (File.Exists(data.InstallerPath))
                    {
                        StopSendStatus();
                        if (Helpers.StartHiddenProcess(data.InstallerPath, data.InstallerArgs) != 0)
                        {
                            SendMessage("Update failed!");
                            // Перезапуск службы
                            Helpers.StartHiddenProcess("cmd", "/c net stop ccdiag & net start ccdiag");
                        }
                    }
                    else
                    {
                        SendMessage("Updater executable not found!");
                    }
                }
            }
        }

        public event EventHandler<ControlClientEventArgs> DataArrived; 

        protected virtual void OnDataArrived(ControlMessage packet)
        {
            var handler = DataArrived;
            if (handler != null)
            {
                handler(this, new ControlClientEventArgs() { Packet = packet });
            }
        }

        public void SendMessage(string text)
        {
            var packet = new ControlMessage() { Message = text, Version = Maintenance.CurrentVersion.ToString() };
            SendPacket(packet);
        }

        public void SendPacket(ControlMessage packet)
        {
            var data = Helpers.ObjToBytes(packet);
            _client.Send(data, data.Length);
        }

        private SystemStateData _sysData;

        public SystemStateData SysData
        {
            get { return _sysData ?? (_sysData = new SystemStateData()); }
        }

        private bool BeginSendStatus()
        {
            if (_sending)
                return true;
            if (!CheckUdpClient())
                return false;
            _sending = true;
            ThreadPool.QueueUserWorkItem(obj =>
                {
                    var packet = new ControlMessage() { Data = SysData, DataType = "SystemState" };
                    while (!_stopRequested)
                    {
                        SysData.UpdateValues();
                        SendPacket(packet);
                        Thread.Sleep(_delay);
                    }
                    _stopRequested = false;
                    _sending = false;
                });
            return true;
        }

        private void StartReplyListener(Action<byte[]> processing)
        {
            ThreadPool.QueueUserWorkItem(obj =>
            {
                var localEP = _client.Client.LocalEndPoint as IPEndPoint;
                IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
                while (!_stopRequested)
                {
                    try
                    {
                        var bytes = _client.Receive(ref sender);
                        (obj as Action<byte[]>)(bytes);
                    }
                    catch { }
                }
            }, processing);
        }

        public void StopSendStatus()
        {
            _stopRequested = true;
        }

        ~ControlClient()
        {
            if (_client != null)
                _client.Close();
        }
    }
}
