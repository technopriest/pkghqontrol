﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace QontrolService
{
    public class Program
    {
        static void Main()
        {
            var args = Environment.GetCommandLineArgs();

            bool maintenanceMode = false;

            if (Helpers.CmdLnArgSet("remove"))
            {
                Maintenance.ServiceRemove();
                maintenanceMode = true;
            }

            if (Helpers.CmdLnArgSet("install"))
            {
                Maintenance.ServiceInstall(Helpers.GetCmdLnArg("install"));
                maintenanceMode = true;
            }

            if (Helpers.CmdLnArgSet("verify"))
            {
                maintenanceMode = true;
                Maintenance.Verify();
            }

            if (Helpers.CmdLnArgSet("console"))
            {
                ConsoleMain(args);
            }
            else if (!maintenanceMode && Helpers.CmdLnArgSet("service"))
            {
                ServiceMain();
            }
        }

        public static void ServiceMain()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
                { 
                    new Service() 
                };
            ServiceBase.Run(ServicesToRun);
        }

        public static void ConsoleMain(string[] args)
        {
            if (Helpers.CmdLnArgSet("console"))
                Console.Title = "Service Debug";

            if (Helpers.CmdLnArgSet("server"))
                Server();

            string host = Helpers.GetCmdLnArg("client");
            if (host != null)
                Client(host);

            SystemWriteLine("Press <ENTER> to terminate");
            if (Helpers.CmdLnArgSet("console"))
                Console.ReadLine();
        }

        private static ControlServer _server = null;
        public static ControlServer QontrolServer { get { return _server; } }

        static void Server()
        {
            int i = 0;
            _server = new ControlServer(9988);
            _server.DataArrived += DataArrivedOnServer;
            if (!_server.Start()) 
                ServerWriteLine("Socket bind failed!");
            else ServerWriteLine("Listening on " + _server.ToString());
        }

        private static void DataArrivedOnServer(object sender, ServerEventArgs e)
        {
            Program.SystemWriteLine(e.Client.ToString());
            Program.ServerWriteLine(e.Packet.Data.ToString());
            if (e.Packet.Message != null)
                ClientWriteLine(e.Packet.Message);
        }

        private static ControlClient _statusSender = null;
        public static ControlClient QontrolClient { get { return _statusSender; } }

        static void Client(string host)
        {
            _statusSender = new ControlClient(host, 9988, 500);
            _statusSender.DataArrived += DataArrivedFromServer;
            if (!_statusSender.Init())
            {
                ClientWriteLine("Can not find host!");
            }
        }

        private static void DataArrivedFromServer(object sender, ControlClientEventArgs e)
        {
            if (e.Packet.Message == "reboot")
            {
                Helpers.StartHiddenProcess("shutdown", "-r -f -t 0");
            }
        }

        public static void ServerWriteLine(string format, params object[] args)
        {
            if (!Helpers.CmdLnArgSet("console"))
                return;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(format, args);
        }

        public static void ClientWriteLine(string format, params object[] args)
        {
            if (!Helpers.CmdLnArgSet("console"))
                return;
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(format, args);
        }

        public static void SystemWriteLine(string format, params object[] args)
        {
            if (!Helpers.CmdLnArgSet("console"))
                return;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(format, args);
        }
    }
}
