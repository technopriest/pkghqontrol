﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace QontrolService
{
    public static class Helpers
    {
        public static string RegexMatch(this string input, string pattern, int groupNum = 1)
        {
            var match = Regex.Match(input, pattern);
            if (match.Success)
            {
                return match.Groups[groupNum].Value;
            }
            return null;
        }
        public static TResult IfNotNull<T, TResult>(this T target, Func<T, TResult> getValue)
        {
            if (target != null)
                return getValue(target);
            else
                return default(TResult);
        }
        public static byte[] ObjToBytes(object obj)
        {
            var bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                using (var gzStream = new GZipStream(ms, CompressionMode.Compress, true))
                {
                    bf.Serialize(gzStream, obj);
                }
                return ms.ToArray();
            }
        }

        public static T BytesToObj<T>(byte[] bytes)
        {
            T result = default(T);
            var bf = new BinaryFormatter();
            using (var ms = new MemoryStream(bytes))
            {
                using (var gzStream = new GZipStream(ms, CompressionMode.Decompress))
                {
                    result = (T)bf.Deserialize(gzStream);
                }
            }
            return result;
        }

        public static IPAddress GetIPAddress(string hostOrIp)
        {
            IPAddress result;
            if (IPAddress.TryParse(hostOrIp, out result))
                return result;
            try
            {
                return Dns.GetHostAddresses(hostOrIp)[0];
            }
            catch
            {
                return null;
            }
        }

        public static string GetCmdLnArg(string key)
        {
            var args = Environment.GetCommandLineArgs();
            for (int i = 0; i < args.Length; i++)
            {
                var arg = args[i];
                if (Regex.Match(arg, "[/-]" + key, RegexOptions.IgnoreCase).Success)
                {
                    if (i + 1 < args.Length)
                    {
                        return args[i + 1];
                    }
                    return null;
                }
            }
            return null;
        }

        public static bool HasAdminRights
        {
            get { return new WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator); }
        }

        public static bool CmdLnArgSet(string key)
        {
            var args = Environment.GetCommandLineArgs();
            return args.Any(arg => Regex.Match(arg, "[/-]" + key, RegexOptions.IgnoreCase).Success);
        }

        public static int StartHiddenProcess(string command, string args)
        {
            var psi = new ProcessStartInfo(command, args) { CreateNoWindow = true, WindowStyle = ProcessWindowStyle.Hidden };
            try
            {
                var proc = Process.Start(psi);
                proc.WaitForExit();
                return proc.ExitCode;
            }
            catch
            {
                return -1;
            }
        }

        public static string ReadConsoleProcessOutput(string command, string args)
        {
            var buffer = new StringBuilder(1024);
            CreateProcessWithRedirectedOutput(command, args, 
                line => buffer.AppendLine(line), true);
            return buffer.ToString();
        }

        public static Process CreateProcessWithRedirectedOutput(string command, string args, Action<string> onReadLine = null, bool waitForExit = false)
        {
            var psi = new ProcessStartInfo(command, args)
            {
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                RedirectStandardOutput = true,
                StandardOutputEncoding = Encoding.GetEncoding(866),
                UseShellExecute = false
            };
            var proc = new Process();
            proc.StartInfo = psi;
            if (onReadLine != null)
            {
                proc.OutputDataReceived += (s, d) => onReadLine(d.Data);
                try
                {
                    proc.Start();
                    proc.BeginOutputReadLine();
                    if (waitForExit)
                        proc.WaitForExit();
                }
                catch { }
            }
            return proc;
        }

        public static T CreateClassFromAssembly<T>(string assemblyPath, string className)
        {
            var assembly = Assembly.LoadFrom(assemblyPath);
            var type = assembly.GetType(className);
            return (T)Activator.CreateInstance(type);
        }

    }
}
